Header-Only Hashlib++
=====================

This project implements a header-only hash library. It is based on [Hashlib++](http://hashlib2plus.sourceforge.net/).

By now hashlib++ supports the following algorithms:

- **MD5:** Message-Digest algorithm 5
- **SHA1:** Secure Hash Algorithm 1
- **SHA2-Family:** Secure Hash Algorithm *256*, *384* and *512*


# Install

Copy `ho-hashlibpp.h` available at `source/Lib` into your include directory, and you are set.  

# Tests

To build the tests, checkout the repository and use CMake:

```bash
mkdir build
cd build
cmake ..
make
GTEST_COLOR=1 CTEST_OUTPUT_ON_FAILURE=1 ctest --verbose
```

# Usage

Check the tests at `source/Tests/*.cpp` to see how to use the library. Basically, all you need is to import the header and call one of the following functions:

- `std::string MD5(std::string input)`
- `std::string MD5(FILE *input)`
- `std::string MD5(std::istream &input)`
- `std::string SHA1(std::string input) `
- `std::string SHA1(FILE *input)`
- `std::string SHA1(std::istream &input)`
- `std::string SHA256(std::string input) `
- `std::string SHA256(FILE *input)`
- `std::string SHA256(std::istream &input) `
- `std::string SHA384(std::string input) `
- `std::string SHA384(FILE *input)`
- `std::string SHA384(std::istream &input)`
- `std::string SHA512(std::string input)`
- `std::string SHA512(FILE *input)`
- `std::string SHA512(std::istream &input)`

## Example

```cpp
#include "ho-hashlibpp.h"
#include <cassert>
#include <string>

int main(int argc, char* argv[]) {
  std::string hash = HO_Hashlibpp::SHA1("Hello World");
  assert(hash == "0a4d55a8d778e5022fab701977c5d840bbc486d0");
  return 0;
}
```

## License

This library is available under a 2-Clause BSD license. See the [LICENSE.md](LICENSE.md) file for details.
