# NOTICES

This software uses the following Open Source components:

* **"Google Test"** version 1.10.0, available at [https://github.com/google/googletest](https://github.com/google/googletest) and distributed under a **BSD 3-Clause "New" or "Revised" License**. Copyright (c) 2008 Google Inc. All rights reserved.

* **"hashlib++"** version 0.3.4, available at [https://github.com/aksalj/hashlibpp](https://github.com/aksalj/hashlibpp) and distributed under a **BSD 2-Clause License**. Copyright (c) 2007-2011 Benjamin Grudelbach. All rights reserved.
