/* The copyright in this software is being made available under the BSD
 * License, included below. This software may be subject to other third party
 * and contributor rights, including patent rights, and no such rights are
 * granted under this license.
 *
 * Copyright (c) 2020, Pedro Garcia Freitas
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  * Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *  * Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

/** \file     ho-hashlibpp.h
 *  \brief    Standalone header-only library to compute hash functions. 
 *  \author   Pedro Garcia Freitas <pedrogarcia@ieee.org>
 *  \date     2020-04-04
 */

#ifndef HO_HASHLIBPP_LIB_HO_HASHLIBPP__H__
#define HO_HASHLIBPP_LIB_HO_HASHLIBPP__H__

#include <cassert>
#include <cstring>
#include <fstream>
#include <iostream>
#include <memory>
#include <sstream>
#include <string>
#include <vector>


namespace HO_Hashlibpp {
namespace ___ {
  typedef unsigned char hl_uint8;
  typedef unsigned short int hl_uint16;
  typedef unsigned int hl_uint32;
  typedef unsigned long int hl_uint48;
  typedef hl_uint8 *POINTER;
  typedef unsigned int custom_uint;

  constexpr custom_uint S11 = 7;
  constexpr custom_uint S12 = 12;
  constexpr custom_uint S13 = 17;
  constexpr custom_uint S14 = 22;
  constexpr custom_uint S21 = 5;
  constexpr custom_uint S22 = 9;
  constexpr custom_uint S23 = 14;
  constexpr custom_uint S24 = 20;
  constexpr custom_uint S31 = 4;
  constexpr custom_uint S32 = 11;
  constexpr custom_uint S33 = 16;
  constexpr custom_uint S34 = 23;
  constexpr custom_uint S41 = 6;
  constexpr custom_uint S42 = 10;
  constexpr custom_uint S43 = 15;
  constexpr custom_uint S44 = 21;


  typedef struct {
    hl_uint48 state[4];
    hl_uint48 count[2];
    unsigned char buffer[64];
  } HL_MD5_CTX;


  typedef enum hlerrors {
    HL_NO_ERROR = 0,
    HL_FILE_READ_ERROR,
    HL_VERIFY_TEST_FAILED,
    HL_UNKNOWN_SEE_MSG,
    HL_UNKNOWN_HASH_TYPE
  } hlerror;


#ifdef __GNUC__
  typedef unsigned long long int hl_uint64;
#elif __MINGW32__
  typedef unsigned long long int hl_uint64;
#elif _MSC_VER
  typedef unsigned __int64 hl_uint64;
#else
#error \
    "Unsuppported compiler." \
               "Please use GCC,MINGW,MSVC " \
         " or define hl_uint64 for your compiler"
#endif


  static unsigned char PADDING[64] = {0x80, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
      0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
      0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
      0};


  template<typename X, typename Y, typename Z>
  constexpr auto F(X x, Y y, Z z) {
    return (((x) & (y)) | ((~x) & (z)));
  }


  template<typename X, typename Y, typename Z>
  constexpr auto G(X x, Y y, Z z) {
    return (((x) & (z)) | ((y) & (~z)));
  }


  template<typename X, typename Y, typename Z>
  constexpr auto H(X x, Y y, Z z) {
    return ((x) ^ (y) ^ (z));
  }


  template<typename X, typename Y, typename Z>
  constexpr auto I(X x, Y y, Z z) {
    return ((y) ^ ((x) | (~z)));
  }


  template<typename X, typename N>
  constexpr auto ROTATE_LEFT(X x, N n) {
    return (((x) << (n)) | (((unsigned int) x) >> (32 - (n))));
  }


  template<typename A, typename B, typename C, typename D, typename X,
      typename S, typename AC>
  constexpr auto FF(A &a, B &b, C &c, D &d, X &x, S s, AC ac) {
    (a) += F((b), (c), (d)) + (x) + (hl_uint48)(ac);
    (a) = ROTATE_LEFT((a), (s));
    (a) += (b);
  }


  template<typename A, typename B, typename C, typename D, typename X,
      typename S, typename AC>
  constexpr auto GG(A &a, B &b, C &c, D &d, X &x, S s, AC ac) {
    (a) += G((b), (c), (d)) + (x) + (hl_uint48)(ac);
    (a) = ROTATE_LEFT((a), (s));
    (a) += (b);
  }


  template<typename A, typename B, typename C, typename D, typename X,
      typename S, typename AC>
  constexpr auto HH(A &a, B &b, C &c, D &d, X &x, S s, AC ac) {
    (a) += H((b), (c), (d)) + (x) + (hl_uint48)(ac);
    (a) = ROTATE_LEFT((a), (s));
    (a) += (b);
  }


  template<typename A, typename B, typename C, typename D, typename X,
      typename S, typename AC>
  constexpr void II(A &a, B &b, C &c, D &d, X &x, S s, AC ac) {
    (a) += I((b), (c), (d)) + (x) + (hl_uint48)(ac);
    (a) = ROTATE_LEFT((a), (s));
    (a) += (b);
  }


  constexpr hl_uint32 SHA256_BLOCK_LENGTH = 64;
  constexpr hl_uint32 SHA256_SHORT_BLOCK_LENGTH = SHA256_BLOCK_LENGTH - 8;
  constexpr hl_uint32 SHA256_DIGEST_LENGTH = 32;
  constexpr hl_uint32 SHA256_DIGEST_STRING_LENGTH =
      SHA256_DIGEST_LENGTH * 2 + 1;

  typedef hl_uint8 sha2_byte;
  typedef hl_uint32 sha2_word32;
  typedef hl_uint64 sha2_word64;

  typedef struct HL_SHA256_CTX {
    hl_uint32 state[8];
    hl_uint64 bitcount;
    hl_uint8 buffer[SHA256_BLOCK_LENGTH];
  } HL_SHA256_CTX;


//#define LITTLE_ENDIAN 1234
//#define BIG_ENDIAN    4321
#define MACHINE_BYTE_ORDER LITTLE_ENDIAN
#if !defined(MACHINE_BYTE_ORDER) || \
    (MACHINE_BYTE_ORDER != LITTLE_ENDIAN && MACHINE_BYTE_ORDER != BIG_ENDIAN)
#error Define BYTE_ORDER to be equal to either LITTLE_ENDIAN or BIG_ENDIAN
#endif

/*** ENDIAN REVERSAL MACROS *******************************************/
#if MACHINE_BYTE_ORDER == LITTLE_ENDIAN

  template<typename W, typename X>
  constexpr void REVERSE32(W w, X &x) {
    sha2_word32 tmp = (w);
    tmp = (tmp >> 16) | (tmp << 16);
    (x) = ((tmp & 0xff00ff00UL) >> 8) | ((tmp & 0x00ff00ffUL) << 8);
  }

  template<typename W, typename X>
  constexpr auto REVERSE64(W w, X &x) {
    sha2_word64 tmp = (w);
    tmp = (tmp >> 32) | (tmp << 32);
    tmp = ((tmp & 0xff00ff00ff00ff00ULL) >> 8) |
          ((tmp & 0x00ff00ff00ff00ffULL) << 8);
    (x) = ((tmp & 0xffff0000ffff0000ULL) >> 16) |
          ((tmp & 0x0000ffff0000ffffULL) << 16);
  }
#endif /* MACHINE_BYTE_ORDER ==   LITTLE_ENDIAN */


  template<typename W, typename N>
  constexpr auto ADDINC128(W *w, N n) {
    (w)[0] += (sha2_word64)(n);
    if ((w)[0] < (sha2_word64)(n)) {
      (w)[1]++;
    }
  }

/*
 * Macros for copying blocks of memory and for zeroing out ranges
 * of memory.  Using these macros makes it easy to switch from
 * using memset()/memcpy() and using bzero()/bcopy().
 *
 * Please define either SHA2_USE_MEMSET_MEMCPY or define
 * SHA2_USE_BZERO_BCOPY depending on which function set you
 * choose to use:
 */
#if !defined(SHA2_USE_MEMSET_MEMCPY) && !defined(SHA2_USE_BZERO_BCOPY)
/* Default to memset()/memcpy() if no option is specified */
#define SHA2_USE_MEMSET_MEMCPY 1
#endif
#if defined(SHA2_USE_MEMSET_MEMCPY) && defined(SHA2_USE_BZERO_BCOPY)
/* Abort with an error if BOTH options are defined */
#error Define either SHA2_USE_MEMSET_MEMCPY or SHA2_USE_BZERO_BCOPY, not both!
#endif

#ifdef SHA2_USE_MEMSET_MEMCPY

  template<typename P, typename L>
  constexpr auto MEMSET_BZERO(P p, L l) {
    memset((p), 0, (l));
  }

  template<typename D, typename S, typename L>
  constexpr auto MEMCPY_BCOPY(D d, S s, L l) {
    memcpy((d), (s), (l));
  }

#endif

#ifdef SHA2_USE_BZERO_BCOPY

  template<typename P, typename L>
  constexpr auto MEMSET_BZERO(P p, L l) {
    bzero((p), (l))
  }

  template<typename D, typename S, typename L>
  constexpr auto MEMCPY_BCOPY(D d, S s, L l) {
    bcopy((s), (d), (l));
  }
#endif


  template<typename B, typename X>
  constexpr auto SHIFT_RIGHT(B b, X &x) {
    return x >> b;
  }


  template<typename B, typename X>
  constexpr auto SHIFT32_BITS(B b, X &x) {
    return (((x) >> (b)) | ((x) << (32 - (b))));
  }


  template<typename B, typename X>
  constexpr auto SHIFT64_BITS(B b, X &x) {
    return (((x) >> (b)) | ((x) << (64 - (b))));
  }


  template<typename X, typename Y, typename Z>
  constexpr auto Ch(X x, Y y, Z z) {
    return (((x) & (y)) ^ ((~(x)) & (z)));
  }


  template<typename X, typename Y, typename Z>
  constexpr auto Maj(X x, Y y, Z z) {
    return (((x) & (y)) ^ ((x) & (z)) ^ ((y) & (z)));
  }


  template<typename X>
  constexpr auto Sigma0_256(X x) {
    auto s2 = SHIFT32_BITS(2, (x));
    auto s13 = SHIFT32_BITS(13, (x));
    auto s22 = SHIFT32_BITS(22, (x));
    return s2 ^ s13 ^ s22;
  }


  template<typename X>
  constexpr auto Sigma1_256(X x) {
    return SHIFT32_BITS(6, (x)) ^ SHIFT32_BITS(11, (x)) ^ SHIFT32_BITS(25, (x));
  }


  template<typename X>
  constexpr auto sigma0_256(X x) {
    return SHIFT32_BITS(7, (x)) ^ SHIFT32_BITS(18, (x)) ^ SHIFT_RIGHT(3, (x));
  }


  template<typename X>
  constexpr auto sigma1_256(X x) {
    return SHIFT32_BITS(17, (x)) ^ SHIFT32_BITS(19, (x)) ^ SHIFT_RIGHT(10, (x));
  }


  const static sha2_word32 K256[64] = {0x428a2f98UL, 0x71374491UL, 0xb5c0fbcfUL,
      0xe9b5dba5UL, 0x3956c25bUL, 0x59f111f1UL, 0x923f82a4UL, 0xab1c5ed5UL,
      0xd807aa98UL, 0x12835b01UL, 0x243185beUL, 0x550c7dc3UL, 0x72be5d74UL,
      0x80deb1feUL, 0x9bdc06a7UL, 0xc19bf174UL, 0xe49b69c1UL, 0xefbe4786UL,
      0x0fc19dc6UL, 0x240ca1ccUL, 0x2de92c6fUL, 0x4a7484aaUL, 0x5cb0a9dcUL,
      0x76f988daUL, 0x983e5152UL, 0xa831c66dUL, 0xb00327c8UL, 0xbf597fc7UL,
      0xc6e00bf3UL, 0xd5a79147UL, 0x06ca6351UL, 0x14292967UL, 0x27b70a85UL,
      0x2e1b2138UL, 0x4d2c6dfcUL, 0x53380d13UL, 0x650a7354UL, 0x766a0abbUL,
      0x81c2c92eUL, 0x92722c85UL, 0xa2bfe8a1UL, 0xa81a664bUL, 0xc24b8b70UL,
      0xc76c51a3UL, 0xd192e819UL, 0xd6990624UL, 0xf40e3585UL, 0x106aa070UL,
      0x19a4c116UL, 0x1e376c08UL, 0x2748774cUL, 0x34b0bcb5UL, 0x391c0cb3UL,
      0x4ed8aa4aUL, 0x5b9cca4fUL, 0x682e6ff3UL, 0x748f82eeUL, 0x78a5636fUL,
      0x84c87814UL, 0x8cc70208UL, 0x90befffaUL, 0xa4506cebUL, 0xbef9a3f7UL,
      0xc67178f2UL};


  const static sha2_word32 sha256_initial_hash_value[8] = {0x6a09e667UL,
      0xbb67ae85UL, 0x3c6ef372UL, 0xa54ff53aUL, 0x510e527fUL, 0x9b05688cUL,
      0x1f83d9abUL, 0x5be0cd19UL};


  static const char *sha2_hex_digits = "0123456789abcdef";


  constexpr auto SHA384_BLOCK_LENGTH = 128;
  constexpr auto SHA384_DIGEST_LENGTH = 48;
  constexpr auto SHA384_DIGEST_STRING_LENGTH = SHA384_DIGEST_LENGTH * 2 + 1;
  constexpr auto SHA512_BLOCK_LENGTH = 128;
  constexpr auto SHA512_DIGEST_LENGTH = 64;
  constexpr auto SHA512_DIGEST_STRING_LENGTH = SHA512_DIGEST_LENGTH * 2 + 1;
  constexpr auto SHA512_SHORT_BLOCK_LENGTH = SHA512_BLOCK_LENGTH - 16;

  typedef hl_uint8 sha2_byte;
  typedef hl_uint32 sha2_word32;
  typedef hl_uint64 sha2_word64;


  typedef struct HL_SHA512_CTX {
    hl_uint64 state[8];
    hl_uint64 bitcount[2];
    hl_uint8 buffer[SHA512_BLOCK_LENGTH];
  } HL_SHA512_CTX;


  typedef HL_SHA512_CTX HL_SHA_384_CTX;


  const static sha2_word64 K512[80] = {0x428a2f98d728ae22ULL,
      0x7137449123ef65cdULL, 0xb5c0fbcfec4d3b2fULL, 0xe9b5dba58189dbbcULL,
      0x3956c25bf348b538ULL, 0x59f111f1b605d019ULL, 0x923f82a4af194f9bULL,
      0xab1c5ed5da6d8118ULL, 0xd807aa98a3030242ULL, 0x12835b0145706fbeULL,
      0x243185be4ee4b28cULL, 0x550c7dc3d5ffb4e2ULL, 0x72be5d74f27b896fULL,
      0x80deb1fe3b1696b1ULL, 0x9bdc06a725c71235ULL, 0xc19bf174cf692694ULL,
      0xe49b69c19ef14ad2ULL, 0xefbe4786384f25e3ULL, 0x0fc19dc68b8cd5b5ULL,
      0x240ca1cc77ac9c65ULL, 0x2de92c6f592b0275ULL, 0x4a7484aa6ea6e483ULL,
      0x5cb0a9dcbd41fbd4ULL, 0x76f988da831153b5ULL, 0x983e5152ee66dfabULL,
      0xa831c66d2db43210ULL, 0xb00327c898fb213fULL, 0xbf597fc7beef0ee4ULL,
      0xc6e00bf33da88fc2ULL, 0xd5a79147930aa725ULL, 0x06ca6351e003826fULL,
      0x142929670a0e6e70ULL, 0x27b70a8546d22ffcULL, 0x2e1b21385c26c926ULL,
      0x4d2c6dfc5ac42aedULL, 0x53380d139d95b3dfULL, 0x650a73548baf63deULL,
      0x766a0abb3c77b2a8ULL, 0x81c2c92e47edaee6ULL, 0x92722c851482353bULL,
      0xa2bfe8a14cf10364ULL, 0xa81a664bbc423001ULL, 0xc24b8b70d0f89791ULL,
      0xc76c51a30654be30ULL, 0xd192e819d6ef5218ULL, 0xd69906245565a910ULL,
      0xf40e35855771202aULL, 0x106aa07032bbd1b8ULL, 0x19a4c116b8d2d0c8ULL,
      0x1e376c085141ab53ULL, 0x2748774cdf8eeb99ULL, 0x34b0bcb5e19b48a8ULL,
      0x391c0cb3c5c95a63ULL, 0x4ed8aa4ae3418acbULL, 0x5b9cca4f7763e373ULL,
      0x682e6ff3d6b2b8a3ULL, 0x748f82ee5defb2fcULL, 0x78a5636f43172f60ULL,
      0x84c87814a1f0ab72ULL, 0x8cc702081a6439ecULL, 0x90befffa23631e28ULL,
      0xa4506cebde82bde9ULL, 0xbef9a3f7b2c67915ULL, 0xc67178f2e372532bULL,
      0xca273eceea26619cULL, 0xd186b8c721c0c207ULL, 0xeada7dd6cde0eb1eULL,
      0xf57d4f7fee6ed178ULL, 0x06f067aa72176fbaULL, 0x0a637dc5a2c898a6ULL,
      0x113f9804bef90daeULL, 0x1b710b35131c471bULL, 0x28db77f523047d84ULL,
      0x32caab7b40c72493ULL, 0x3c9ebe0a15c9bebcULL, 0x431d67c49c100d4cULL,
      0x4cc5d4becb3e42b6ULL, 0x597f299cfc657e2aULL, 0x5fcb6fab3ad6faecULL,
      0x6c44198c4a475817ULL};

  const static sha2_word64 sha384_initial_hash_value[8] = {
      0xcbbb9d5dc1059ed8ULL, 0x629a292a367cd507ULL, 0x9159015a3070dd17ULL,
      0x152fecd8f70e5939ULL, 0x67332667ffc00b31ULL, 0x8eb44a8768581511ULL,
      0xdb0c2e0d64f98fa7ULL, 0x47b5481dbefa4fa4ULL};

  const static sha2_word64 sha512_initial_hash_value[8] = {
      0x6a09e667f3bcc908ULL, 0xbb67ae8584caa73bULL, 0x3c6ef372fe94f82bULL,
      0xa54ff53a5f1d36f1ULL, 0x510e527fade682d1ULL, 0x9b05688c2b3e6c1fULL,
      0x1f83d9abfb41bd6bULL, 0x5be0cd19137e2179ULL};


  template<typename X>
  constexpr auto Sigma0_512(X x) {
    auto s28 = SHIFT64_BITS(28, (x));
    auto s34 = SHIFT64_BITS(34, (x));
    auto s39 = SHIFT64_BITS(39, (x));
    return s28 ^ s34 ^ s39;
  }


  template<typename X>
  constexpr auto Sigma1_512(X x) {
    auto s14 = SHIFT64_BITS(14, (x));
    auto s18 = SHIFT64_BITS(18, (x));
    auto s41 = SHIFT64_BITS(41, (x));
    return s14 ^ s18 ^ s41;
  }


  template<typename X>
  constexpr auto sigma0_512(X x) {
    return SHIFT64_BITS(1, (x)) ^ SHIFT64_BITS(8, (x)) ^ SHIFT_RIGHT(7, (x));
  }


  template<typename X>
  constexpr auto sigma1_512(X x) {
    return SHIFT64_BITS(19, (x)) ^ SHIFT64_BITS(61, (x)) ^ SHIFT_RIGHT(6, (x));
  }


  class hlException {
   private:
    hlerror iError;
    std::string strMessge;


   public:
    hlException(hlerror er, std::string m) {
      this->iError = er;
      this->strMessge = m;
    }


    hlException(std::string m) {
      this->iError = HL_UNKNOWN_SEE_MSG;
      this->strMessge = m;
    }


    std::string error_message(void) {
      return strMessge;
    }


    hlerror error_number(void) {
      return iError;
    }
  };


#ifndef _SHA_enum_
#define _SHA_enum_
  enum {
    shaSuccess = 0,
    shaNull, /* Null pointer parameter */
    shaInputTooLong, /* input data too long */
    shaStateError /* called Input after Result */
  };
#endif

  constexpr auto SHA1HashSize = 20;


  template<typename B, typename W>
  constexpr auto SHA1CircularShift(B bits, W word) {
    return (((word) << (bits)) | ((word) >> (32 - (bits))));
  }


  typedef struct HL_SHA1_CTX {
    /** Message Digest */
    hl_uint32 Intermediate_Hash[SHA1HashSize / 4];

    /** Message length in bits */
    hl_uint32 Length_Low;

    /** Message length in bits */
    hl_uint32 Length_High;

    /** Index into message block array */
    hl_uint16 Message_Block_Index;

    /** 512-bit message blocks */
    hl_uint8 Message_Block[64];

    /** Is the digest computed? */
    int Computed;

    /** Is the message digest corrupted? */
    int Corrupted;

  } HL_SHA1_CTX;


  class MD5 {
   private:
    void MD5Transform(hl_uint48 state[4], unsigned char block[64]) {
      hl_uint48 a = state[0], b = state[1], c = state[2], d = state[3];
      hl_uint48 x[16];

      Decode(x, block, 64);

      /* Round 1 */
      FF(a, b, c, d, x[0], S11, 0xd76aa478); /* 1 */
      FF(d, a, b, c, x[1], S12, 0xe8c7b756); /* 2 */
      FF(c, d, a, b, x[2], S13, 0x242070db); /* 3 */
      FF(b, c, d, a, x[3], S14, 0xc1bdceee); /* 4 */
      FF(a, b, c, d, x[4], S11, 0xf57c0faf); /* 5 */
      FF(d, a, b, c, x[5], S12, 0x4787c62a); /* 6 */
      FF(c, d, a, b, x[6], S13, 0xa8304613); /* 7 */
      FF(b, c, d, a, x[7], S14, 0xfd469501); /* 8 */
      FF(a, b, c, d, x[8], S11, 0x698098d8); /* 9 */
      FF(d, a, b, c, x[9], S12, 0x8b44f7af); /* 10 */
      FF(c, d, a, b, x[10], S13, 0xffff5bb1); /* 11 */
      FF(b, c, d, a, x[11], S14, 0x895cd7be); /* 12 */
      FF(a, b, c, d, x[12], S11, 0x6b901122); /* 13 */
      FF(d, a, b, c, x[13], S12, 0xfd987193); /* 14 */
      FF(c, d, a, b, x[14], S13, 0xa679438e); /* 15 */
      FF(b, c, d, a, x[15], S14, 0x49b40821); /* 16 */

      /* Round 2 */
      GG(a, b, c, d, x[1], S21, 0xf61e2562); /* 17 */
      GG(d, a, b, c, x[6], S22, 0xc040b340); /* 18 */
      GG(c, d, a, b, x[11], S23, 0x265e5a51); /* 19 */
      GG(b, c, d, a, x[0], S24, 0xe9b6c7aa); /* 20 */
      GG(a, b, c, d, x[5], S21, 0xd62f105d); /* 21 */
      GG(d, a, b, c, x[10], S22, 0x2441453); /* 22 */
      GG(c, d, a, b, x[15], S23, 0xd8a1e681); /* 23 */
      GG(b, c, d, a, x[4], S24, 0xe7d3fbc8); /* 24 */
      GG(a, b, c, d, x[9], S21, 0x21e1cde6); /* 25 */
      GG(d, a, b, c, x[14], S22, 0xc33707d6); /* 26 */
      GG(c, d, a, b, x[3], S23, 0xf4d50d87); /* 27 */

      GG(b, c, d, a, x[8], S24, 0x455a14ed); /* 28 */
      GG(a, b, c, d, x[13], S21, 0xa9e3e905); /* 29 */
      GG(d, a, b, c, x[2], S22, 0xfcefa3f8); /* 30 */
      GG(c, d, a, b, x[7], S23, 0x676f02d9); /* 31 */
      GG(b, c, d, a, x[12], S24, 0x8d2a4c8a); /* 32 */

      /* Round 3 */
      HH(a, b, c, d, x[5], S31, 0xfffa3942); /* 33 */
      HH(d, a, b, c, x[8], S32, 0x8771f681); /* 34 */
      HH(c, d, a, b, x[11], S33, 0x6d9d6122); /* 35 */
      HH(b, c, d, a, x[14], S34, 0xfde5380c); /* 36 */
      HH(a, b, c, d, x[1], S31, 0xa4beea44); /* 37 */
      HH(d, a, b, c, x[4], S32, 0x4bdecfa9); /* 38 */
      HH(c, d, a, b, x[7], S33, 0xf6bb4b60); /* 39 */
      HH(b, c, d, a, x[10], S34, 0xbebfbc70); /* 40 */
      HH(a, b, c, d, x[13], S31, 0x289b7ec6); /* 41 */
      HH(d, a, b, c, x[0], S32, 0xeaa127fa); /* 42 */
      HH(c, d, a, b, x[3], S33, 0xd4ef3085); /* 43 */
      HH(b, c, d, a, x[6], S34, 0x4881d05); /* 44 */
      HH(a, b, c, d, x[9], S31, 0xd9d4d039); /* 45 */
      HH(d, a, b, c, x[12], S32, 0xe6db99e5); /* 46 */
      HH(c, d, a, b, x[15], S33, 0x1fa27cf8); /* 47 */
      HH(b, c, d, a, x[2], S34, 0xc4ac5665); /* 48 */

      /* Round 4 */
      II(a, b, c, d, x[0], S41, 0xf4292244); /* 49 */
      II(d, a, b, c, x[7], S42, 0x432aff97); /* 50 */
      II(c, d, a, b, x[14], S43, 0xab9423a7); /* 51 */
      II(b, c, d, a, x[5], S44, 0xfc93a039); /* 52 */
      II(a, b, c, d, x[12], S41, 0x655b59c3); /* 53 */
      II(d, a, b, c, x[3], S42, 0x8f0ccc92); /* 54 */
      II(c, d, a, b, x[10], S43, 0xffeff47d); /* 55 */
      II(b, c, d, a, x[1], S44, 0x85845dd1); /* 56 */
      II(a, b, c, d, x[8], S41, 0x6fa87e4f); /* 57 */
      II(d, a, b, c, x[15], S42, 0xfe2ce6e0); /* 58 */
      II(c, d, a, b, x[6], S43, 0xa3014314); /* 59 */
      II(b, c, d, a, x[13], S44, 0x4e0811a1); /* 60 */
      II(a, b, c, d, x[4], S41, 0xf7537e82); /* 61 */
      II(d, a, b, c, x[11], S42, 0xbd3af235); /* 62 */
      II(c, d, a, b, x[2], S43, 0x2ad7d2bb); /* 63 */
      II(b, c, d, a, x[9], S44, 0xeb86d391); /* 64 */

      state[0] += a;
      state[1] += b;
      state[2] += c;
      state[3] += d;

      MD5_memset((POINTER) x, 0, sizeof(x));
    }

    void Encode(unsigned char *output, hl_uint48 *input, unsigned int len) {
      for (unsigned int i = 0, j = 0; j < len; i++, j += 4) {
        output[j] = (unsigned char) (input[i] & 0xff);
        output[j + 1] = (unsigned char) ((input[i] >> 8) & 0xff);
        output[j + 2] = (unsigned char) ((input[i] >> 16) & 0xff);
        output[j + 3] = (unsigned char) ((input[i] >> 24) & 0xff);
      }
    }

    void Decode(hl_uint48 *output, unsigned char *input, unsigned int len) {
      for (unsigned int i = 0, j = 0; j < len; i++, j += 4) {
        output[i] = ((hl_uint48) input[j]) | (((hl_uint48) input[j + 1]) << 8) |
                    (((hl_uint48) input[j + 2]) << 16) |
                    (((hl_uint48) input[j + 3]) << 24);
      }
    }


    void MD5_memcpy(POINTER output, POINTER input, unsigned int len) {
      for (unsigned int i = 0; i < len; i++) {
        output[i] = input[i];
      }
    }


    void MD5_memset(POINTER output, int value, unsigned int len) {
      for (unsigned int i = 0; i < len; i++) {
        ((char *) output)[i] = (char) value;
      }
    }

   public:
    void MD5Init(HL_MD5_CTX *context) {
      context->count[0] = context->count[1] = 0;
      context->state[0] = 0x67452301;
      context->state[1] = 0xefcdab89;
      context->state[2] = 0x98badcfe;
      context->state[3] = 0x10325476;
    }


    void MD5Update(
        HL_MD5_CTX *context, unsigned char *input, unsigned int inputLen) {
      unsigned int i = 0;
      unsigned int index = (unsigned int) ((context->count[0] >> 3) & 0x3F);

      if ((context->count[0] += ((hl_uint48) inputLen << 3)) <
          ((hl_uint48) inputLen << 3))
        context->count[1]++;

      context->count[1] += ((hl_uint48) inputLen >> 29);
      unsigned int partLen = 64 - index;

      if (inputLen >= partLen) {
        MD5_memcpy((POINTER) &context->buffer[index], (POINTER) input, partLen);
        MD5Transform(context->state, context->buffer);

        for (i = partLen; i + 63 < inputLen; i += 64)
          MD5Transform(context->state, &input[i]);

        index = 0;
      }

      MD5_memcpy(
          (POINTER) &context->buffer[index], (POINTER) &input[i], inputLen - i);
    }


    void MD5Final(unsigned char digest[16], HL_MD5_CTX *context) {
      unsigned char bits[8];

      /* Save number of bits */
      Encode(bits, context->count, 8);

      /* 
   * Pad out to 56 mod 64.
   */
      unsigned int index = (unsigned int) ((context->count[0] >> 3) & 0x3f);
      unsigned int padLen = (index < 56) ? (56 - index) : (120 - index);
      MD5Update(context, PADDING, padLen);

      /* Append length (before padding) */
      MD5Update(context, bits, 8);

      /* Store state in digest */
      Encode(digest, context->state, 16);

      /*
   * Zeroize sensitive information.
   */
      MD5_memset((POINTER) context, 0, sizeof(*context));
    }

    /**
     *  @brief   default constructor
     */
    MD5(){};
  };


  class SHA1 {
   private:
    void SHA1PadMessage(HL_SHA1_CTX *context) {
      /*
   *  Check to see if the current message block is too small to hold
   *  the initial padding bits and length.  If so, we will pad the
   *  block, process it, and then continue padding into a second
   *  block.
   */
      if (context->Message_Block_Index > 55) {
        context->Message_Block[context->Message_Block_Index++] = 0x80;
        while (context->Message_Block_Index < 64) {
          context->Message_Block[context->Message_Block_Index++] = 0;
        }

        SHA1ProcessMessageBlock(context);

        while (context->Message_Block_Index < 56) {
          context->Message_Block[context->Message_Block_Index++] = 0;
        }
      } else {
        context->Message_Block[context->Message_Block_Index++] = 0x80;
        while (context->Message_Block_Index < 56) {
          context->Message_Block[context->Message_Block_Index++] = 0;
        }
      }

      /*
   *  Store the message length as the last 8 octets
   */
      context->Message_Block[56] = context->Length_High >> 24;
      context->Message_Block[57] = context->Length_High >> 16;
      context->Message_Block[58] = context->Length_High >> 8;
      context->Message_Block[59] = context->Length_High;
      context->Message_Block[60] = context->Length_Low >> 24;
      context->Message_Block[61] = context->Length_Low >> 16;
      context->Message_Block[62] = context->Length_Low >> 8;
      context->Message_Block[63] = context->Length_Low;

      SHA1ProcessMessageBlock(context);
    }


    void SHA1ProcessMessageBlock(HL_SHA1_CTX *context) {
      const hl_uint32 K[] = {/* Constants defined in SHA-1   */
          0x5A827999, 0x6ED9EBA1, 0x8F1BBCDC, 0xCA62C1D6};
      hl_uint32 temp; /* Temporary word value        */
      hl_uint32 W[80]; /* Word sequence               */
      hl_uint32 A, B, C, D, E; /* Word buffers                */

      /*
   *  Initialize the first 16 words in the array W
   */
      for (unsigned int t = 0; t < 16; t++) {
        W[t] = context->Message_Block[t * 4] << 24;
        W[t] |= context->Message_Block[t * 4 + 1] << 16;
        W[t] |= context->Message_Block[t * 4 + 2] << 8;
        W[t] |= context->Message_Block[t * 4 + 3];
      }

      for (unsigned int t = 16; t < 80; t++) {
        W[t] =
            SHA1CircularShift(1, W[t - 3] ^ W[t - 8] ^ W[t - 14] ^ W[t - 16]);
      }

      A = context->Intermediate_Hash[0];
      B = context->Intermediate_Hash[1];
      C = context->Intermediate_Hash[2];
      D = context->Intermediate_Hash[3];
      E = context->Intermediate_Hash[4];

      for (unsigned int t = 0; t < 20; t++) {
        temp =
            SHA1CircularShift(5, A) + ((B & C) | ((~B) & D)) + E + W[t] + K[0];
        E = D;
        D = C;
        C = SHA1CircularShift(30, B);
        B = A;
        A = temp;
      }

      for (unsigned int t = 20; t < 40; t++) {
        temp = SHA1CircularShift(5, A) + (B ^ C ^ D) + E + W[t] + K[1];
        E = D;
        D = C;
        C = SHA1CircularShift(30, B);
        B = A;
        A = temp;
      }

      for (unsigned int t = 40; t < 60; t++) {
        temp = SHA1CircularShift(5, A) + ((B & C) | (B & D) | (C & D)) + E +
               W[t] + K[2];
        E = D;
        D = C;
        C = SHA1CircularShift(30, B);
        B = A;
        A = temp;
      }

      for (unsigned int t = 60; t < 80; t++) {
        temp = SHA1CircularShift(5, A) + (B ^ C ^ D) + E + W[t] + K[3];
        E = D;
        D = C;
        C = SHA1CircularShift(30, B);
        B = A;
        A = temp;
      }

      context->Intermediate_Hash[0] += A;
      context->Intermediate_Hash[1] += B;
      context->Intermediate_Hash[2] += C;
      context->Intermediate_Hash[3] += D;
      context->Intermediate_Hash[4] += E;

      context->Message_Block_Index = 0;
    }

   public:
    int SHA1Reset(HL_SHA1_CTX *context) {
      if (!context) {
        return shaNull;
      }

      context->Length_Low = 0;
      context->Length_High = 0;
      context->Message_Block_Index = 0;

      context->Intermediate_Hash[0] = 0x67452301;
      context->Intermediate_Hash[1] = 0xEFCDAB89;
      context->Intermediate_Hash[2] = 0x98BADCFE;
      context->Intermediate_Hash[3] = 0x10325476;
      context->Intermediate_Hash[4] = 0xC3D2E1F0;

      context->Computed = 0;
      context->Corrupted = 0;

      return shaSuccess;
    }

    int SHA1Input(HL_SHA1_CTX *context, const hl_uint8 *message_array,
        unsigned int length) {
      if (!length) {
        return shaSuccess;
      }

      if (!context || !message_array) {
        return shaNull;
      }

      if (context->Computed) {
        context->Corrupted = shaStateError;
        return shaStateError;
      }

      if (context->Corrupted) {
        return context->Corrupted;
      }
      while (length-- && !context->Corrupted) {
        context->Message_Block[context->Message_Block_Index++] =
            (*message_array & 0xFF);

        context->Length_Low += 8;
        if (context->Length_Low == 0) {
          context->Length_High++;
          if (context->Length_High == 0) {
            /* Message is too long */
            context->Corrupted = 1;
          }
        }

        if (context->Message_Block_Index == 64) {
          SHA1ProcessMessageBlock(context);
        }

        message_array++;
      }

      return shaSuccess;
    }


    int SHA1Result(
        HL_SHA1_CTX *context, hl_uint8 Message_Digest[SHA1HashSize]) {
      if (!context || !Message_Digest) {
        return shaNull;
      }

      if (context->Corrupted) {
        return context->Corrupted;
      }

      if (!context->Computed) {
        SHA1PadMessage(context);
        for (unsigned int i = 0; i < 64; ++i) {
          /* message may be sensitive, clear it out */
          context->Message_Block[i] = 0;
        }
        context->Length_Low = 0; /* and clear length */
        context->Length_High = 0;
        context->Computed = 1;
      }

      for (unsigned int i = 0; i < SHA1HashSize; ++i) {
        Message_Digest[i] =
            context->Intermediate_Hash[i >> 2] >> 8 * (3 - (i & 0x03));
      }

      return shaSuccess;
    }
  };


  class SHA256 {
   private:
    void SHA256_Final(
        hl_uint8 digest[SHA256_DIGEST_LENGTH], HL_SHA256_CTX *context) {
      sha2_word32 *d = reinterpret_cast<sha2_word32 *>(digest);
      unsigned int usedspace;

      /* Sanity check: */
      assert(context != (HL_SHA256_CTX *) 0);

      /* If no digest buffer is passed, we don't bother doing this: */
      if (digest != (sha2_byte *) 0) {
        usedspace = (context->bitcount >> 3) % SHA256_BLOCK_LENGTH;
#if MACHINE_BYTE_ORDER == LITTLE_ENDIAN
        /* Convert FROM host byte order */
        REVERSE64(context->bitcount, context->bitcount);
#endif
        if (usedspace > 0) {
          /* Begin padding with a 1 bit: */
          context->buffer[usedspace++] = 0x80;

          if (usedspace <= SHA256_SHORT_BLOCK_LENGTH) {
            /* Set-up for the last transform: */
            MEMSET_BZERO(&context->buffer[usedspace],
                SHA256_SHORT_BLOCK_LENGTH - usedspace);
          } else {
            if (usedspace < SHA256_BLOCK_LENGTH) {
              MEMSET_BZERO(
                  &context->buffer[usedspace], SHA256_BLOCK_LENGTH - usedspace);
            }
            /* Do second-to-last transform: */
            SHA256_Transform(context, (sha2_word32 *) context->buffer);

            /* And set-up for the last transform: */
            MEMSET_BZERO(context->buffer, SHA256_SHORT_BLOCK_LENGTH);
          }
        } else {
          /* Set-up for the last transform: */
          MEMSET_BZERO(context->buffer, SHA256_SHORT_BLOCK_LENGTH);

          /* Begin padding with a 1 bit: */
          *context->buffer = 0x80;
        }
        /* Set the bit count: */
        *(sha2_word64 *) &context->buffer[SHA256_SHORT_BLOCK_LENGTH] =
            context->bitcount;

        /* Final transform: */
        SHA256_Transform(context, (sha2_word32 *) context->buffer);

#if MACHINE_BYTE_ORDER == LITTLE_ENDIAN
        {
          /* Convert TO host byte order */
          for (unsigned int j = 0; j < 8; j++) {
            REVERSE32(context->state[j], context->state[j]);
            *d++ = context->state[j];
          }
        }
#else
        MEMCPY_BCOPY(d, context->state, SHA256_DIGEST_LENGTH);
#endif
      }

      /* Clean up state data: */
      MEMSET_BZERO(context, sizeof(context));
      usedspace = 0;
    }

    /**
     *  @brief   Internal data transformation
     *  @param  context The context to use
     *  @param  data The data to transform  
     */
    void SHA256_Transform(HL_SHA256_CTX *context, const sha2_word32 *data) {
      sha2_word32 a, b, c, d, e, f, g, h, s0, s1;
      sha2_word32 T1, T2;

      sha2_word32 *W256 = reinterpret_cast<sha2_word32 *>(context->buffer);

      /* Initialize registers with the prev. intermediate value */
      a = context->state[0];
      b = context->state[1];
      c = context->state[2];
      d = context->state[3];
      e = context->state[4];
      f = context->state[5];
      g = context->state[6];
      h = context->state[7];

      unsigned int j = 0;
      do {
#if MACHINE_BYTE_ORDER == LITTLE_ENDIAN
        /* Copy data while converting to host byte order */
        REVERSE32(*data++, W256[j]);
        /* Apply the SHA-256 compression function to update a..h */
        T1 = h + Sigma1_256(e) + Ch(e, f, g) + K256[j] + W256[j];
#else /* MACHINE_BYTE_ORDER == BIG_ENDIAN */
        /* Apply the SHA-256 compression function to update a..h with copy */
        T1 = h + Sigma1_256(e) + Ch(e, f, g) + K256[j] + (W256[j] = *data++);
#endif /* MACHINE_BYTE_ORDER ==  LITTLE_ENDIAN */
        T2 = Sigma0_256(a) + Maj(a, b, c);
        h = g;
        g = f;
        f = e;
        e = d + T1;
        d = c;
        c = b;
        b = a;
        a = T1 + T2;

        j++;
      } while (j < 16);

      do {
        /* Part of the message block expansion: */
        s0 = W256[(j + 1) & 0x0f];
        s0 = sigma0_256(s0);
        s1 = W256[(j + 14) & 0x0f];
        s1 = sigma1_256(s1);

        /* Apply the SHA-256 compression function to update a..h */
        T1 = h + Sigma1_256(e) + Ch(e, f, g) + K256[j] +
             (W256[j & 0x0f] += s1 + W256[(j + 9) & 0x0f] + s0);
        T2 = Sigma0_256(a) + Maj(a, b, c);
        h = g;
        g = f;
        f = e;
        e = d + T1;
        d = c;
        c = b;
        b = a;
        a = T1 + T2;

        j++;
      } while (j < 64);

      /* Compute the current intermediate hash value */
      context->state[0] += a;
      context->state[1] += b;
      context->state[2] += c;
      context->state[3] += d;
      context->state[4] += e;
      context->state[5] += f;
      context->state[6] += g;
      context->state[7] += h;

      /* Clean up */
      a = b = c = d = e = f = g = h = T1 = T2 = 0;
    }

   public:
    /**
     *  @brief   Initialize the context
     *  @param  context The context to init.
     */
    void SHA256_Init(HL_SHA256_CTX *context) {
      if (context == (HL_SHA256_CTX *) 0) {
        return;
      }
      MEMCPY_BCOPY(
          context->state, sha256_initial_hash_value, SHA256_DIGEST_LENGTH);
      MEMSET_BZERO(context->buffer, SHA256_BLOCK_LENGTH);
      context->bitcount = 0;
    }


    void SHA256_Update(
        HL_SHA256_CTX *context, const hl_uint8 *data, unsigned int len) {
      unsigned int freespace, usedspace;

      if (len == 0) {
        /* Calling with no data is valid - we do nothing */
        return;
      }

      /* Sanity check: */
      //assert(context != (HL_SHA256_CTX *) 0 && data != (sha2_byte *) 0);

      usedspace = (context->bitcount >> 3) % SHA256_BLOCK_LENGTH;
      if (usedspace > 0) {
        /* Calculate how much free space is available in the buffer */
        freespace = SHA256_BLOCK_LENGTH - usedspace;

        if (len >= freespace) {
          /* Fill the buffer completely and process it */
          MEMCPY_BCOPY(&context->buffer[usedspace], data, freespace);
          context->bitcount += freespace << 3;
          len -= freespace;
          data += freespace;
          SHA256_Transform(context, (sha2_word32 *) context->buffer);
        } else {
          /* The buffer is not yet full */
          MEMCPY_BCOPY(&context->buffer[usedspace], data, len);
          context->bitcount += len << 3;
          /* Clean up: */
          usedspace = freespace = 0;
          return;
        }
      }
      while (len >= SHA256_BLOCK_LENGTH) {
        /* Process as many complete blocks as we can */
        SHA256_Transform(context, (sha2_word32 *) data);
        context->bitcount += SHA256_BLOCK_LENGTH << 3;
        len -= SHA256_BLOCK_LENGTH;
        data += SHA256_BLOCK_LENGTH;
      }
      if (len > 0) {
        /* There's left-overs, so save 'em */
        MEMCPY_BCOPY(context->buffer, data, len);
        context->bitcount += len << 3;
      }
      usedspace = freespace = 0;
    }

    char *SHA256_End(
        HL_SHA256_CTX *context, char buffer[SHA256_DIGEST_STRING_LENGTH]) {
      sha2_byte digest[SHA256_DIGEST_LENGTH];
      sha2_byte *d = digest;

      /* Sanity check: */
      assert(context != (HL_SHA256_CTX *) 0);

      if (buffer != static_cast<char *>(0)) {
        SHA256_Final(digest, context);

        for (unsigned int i = 0; i < SHA256_DIGEST_LENGTH; i++) {
          *buffer++ = sha2_hex_digits[(*d & 0xf0) >> 4];
          *buffer++ = sha2_hex_digits[*d & 0x0f];
          d++;
        }
        *buffer = static_cast<char>(0);
      } else {
        MEMSET_BZERO(context, sizeof(context));
      }
      MEMSET_BZERO(digest, SHA256_DIGEST_LENGTH);
      return buffer;
    }
  };


  class SHA2ext {
   private:
    void SHA384_Final(
        hl_uint8 digest[SHA384_DIGEST_LENGTH], HL_SHA_384_CTX *context) {
      sha2_word64 *d = reinterpret_cast<sha2_word64 *>(digest);

      /* Sanity check: */
      assert(context != static_cast<HL_SHA_384_CTX *>(0));

      /* If no digest buffer is passed, we don't bother doing this: */
      if (digest != static_cast<sha2_byte *>(0)) {
        SHA512_Last(static_cast<HL_SHA512_CTX *>(context));

        /* Save the hash data for output: */
#if BYTE_ORDER == LITTLE_ENDIAN
        {
          /* Convert TO host byte order */
          int j;
          for (j = 0; j < 6; j++) {
            REVERSE64(context->state[j], context->state[j]);
            *d++ = context->state[j];
          }
        }
#else
        MEMCPY_BCOPY(d, context->state, SHA384_DIGEST_LENGTH);
#endif
      }

      /* Zero out state data */
      MEMSET_BZERO(context, sizeof(context));
    }

    void SHA512_Final(
        hl_uint8 digest[SHA512_DIGEST_LENGTH], HL_SHA512_CTX *context) {
      sha2_word64 *d = reinterpret_cast<sha2_word64 *>(digest);

      assert(context != static_cast<HL_SHA512_CTX *>(0));

      if (digest != static_cast<sha2_byte *>(0)) {
        SHA512_Last(context);

#if BYTE_ORDER == LITTLE_ENDIAN
        {
          for (unsigned int j = 0; j < 8; j++) {
            REVERSE64(context->state[j], context->state[j]);
            *d++ = context->state[j];
          }
        }
#else
        MEMCPY_BCOPY(d, context->state, SHA512_DIGEST_LENGTH);
#endif
      }
      MEMSET_BZERO(context, sizeof(context));
    }

    void SHA512_Last(HL_SHA512_CTX *context) {
      unsigned int usedspace =
          (context->bitcount[0] >> 3) % SHA512_BLOCK_LENGTH;
#if BYTE_ORDER == LITTLE_ENDIAN
      /* Convert FROM host byte order */
      REVERSE64(context->bitcount[0], context->bitcount[0]);
      REVERSE64(context->bitcount[1], context->bitcount[1]);
#endif
      if (usedspace > 0) {
        /* Begin padding with a 1 bit: */
        context->buffer[usedspace++] = 0x80;

        if (usedspace <= SHA512_SHORT_BLOCK_LENGTH) {
          /* Set-up for the last transform: */
          MEMSET_BZERO(&context->buffer[usedspace],
              SHA512_SHORT_BLOCK_LENGTH - usedspace);
        } else {
          if (usedspace < SHA512_BLOCK_LENGTH) {
            MEMSET_BZERO(
                &context->buffer[usedspace], SHA512_BLOCK_LENGTH - usedspace);
          }
          /* Do second-to-last transform: */
          SHA512_Transform(
              context, reinterpret_cast<sha2_word64 *>(context->buffer));

          /* And set-up for the last transform: */
          MEMSET_BZERO(context->buffer, SHA512_BLOCK_LENGTH - 2);
        }
      } else {
        /* Prepare for final transform: */
        MEMSET_BZERO(context->buffer, SHA512_SHORT_BLOCK_LENGTH);

        /* Begin padding with a 1 bit: */
        *context->buffer = 0x80;
      }
      /* Store the length of input data (in bits): */
      *(sha2_word64 *) &context->buffer[SHA512_SHORT_BLOCK_LENGTH] =
          context->bitcount[1];
      *(sha2_word64 *) &context->buffer[SHA512_SHORT_BLOCK_LENGTH + 8] =
          context->bitcount[0];

      /* Final transform: */
      SHA512_Transform(
          context, reinterpret_cast<sha2_word64 *>(context->buffer));
    }

    void SHA512_Transform(HL_SHA512_CTX *context, const sha2_word64 *data) {
      sha2_word64 a, b, c, d, e, f, g, h, s0, s1;
      sha2_word64 T1, T2;
      sha2_word64 *W512 = reinterpret_cast<sha2_word64 *>(context->buffer);

      a = context->state[0];
      b = context->state[1];
      c = context->state[2];
      d = context->state[3];
      e = context->state[4];
      f = context->state[5];
      g = context->state[6];
      h = context->state[7];

      unsigned int j = 0;
      do {
#if BYTE_ORDER == LITTLE_ENDIAN
        REVERSE64(*data++, W512[j]);
        T1 = h + Sigma1_512(e) + Ch(e, f, g) + K512[j] + W512[j];
#else /* BYTE_ORDER == LITTLE_ENDIAN */
        T1 = h + Sigma1_512(e) + Ch(e, f, g) + K512[j] + (W512[j] = *data++);
#endif /* BYTE_ORDER == LITTLE_ENDIAN */
        T2 = Sigma0_512(a) + Maj(a, b, c);
        h = g;
        g = f;
        f = e;
        e = d + T1;
        d = c;
        c = b;
        b = a;
        a = T1 + T2;

        j++;
      } while (j < 16);

      do {
        /* Part of the message block expansion: */
        s0 = W512[(j + 1) & 0x0f];
        s0 = sigma0_512(s0);
        s1 = W512[(j + 14) & 0x0f];
        s1 = sigma1_512(s1);

        /* Apply the SHA-512 compression function to update a..h */
        T1 = h + Sigma1_512(e) + Ch(e, f, g) + K512[j] +
             (W512[j & 0x0f] += s1 + W512[(j + 9) & 0x0f] + s0);
        T2 = Sigma0_512(a) + Maj(a, b, c);
        h = g;
        g = f;
        f = e;
        e = d + T1;
        d = c;
        c = b;
        b = a;
        a = T1 + T2;

        j++;
      } while (j < 80);

      /* Compute the current intermediate hash value */
      context->state[0] += a;
      context->state[1] += b;
      context->state[2] += c;
      context->state[3] += d;
      context->state[4] += e;
      context->state[5] += f;
      context->state[6] += g;
      context->state[7] += h;

      /* Clean up */
      a = b = c = d = e = f = g = h = T1 = T2 = 0;
    }


   public:
    void SHA384_Init(HL_SHA_384_CTX *context) {
      if (context == static_cast<HL_SHA_384_CTX *>(0)) {
        return;
      }
      MEMCPY_BCOPY(
          context->state, sha384_initial_hash_value, SHA512_DIGEST_LENGTH);
      MEMSET_BZERO(context->buffer, SHA384_BLOCK_LENGTH);
      context->bitcount[0] = context->bitcount[1] = 0;
    }

    void SHA512_Init(HL_SHA512_CTX *context) {
      if (context == static_cast<HL_SHA512_CTX *>(0)) {
        return;
      }
      MEMCPY_BCOPY(
          context->state, sha512_initial_hash_value, SHA512_DIGEST_LENGTH);
      MEMSET_BZERO(context->buffer, SHA512_BLOCK_LENGTH);
      context->bitcount[0] = context->bitcount[1] = 0;
    }

    void SHA384_Update(
        HL_SHA_384_CTX *context, const hl_uint8 *data, unsigned int len) {
      SHA512_Update(reinterpret_cast<HL_SHA512_CTX *>(context), data, len);
    }


    void SHA512_Update(
        HL_SHA512_CTX *context, const hl_uint8 *data, unsigned int len) {
      unsigned int freespace, usedspace;

      if (len == 0) {
        return;
      }

      assert(context != static_cast<HL_SHA512_CTX *>(0) &&
             data != static_cast<sha2_byte *>(0));

      usedspace = (context->bitcount[0] >> 3) % SHA512_BLOCK_LENGTH;
      if (usedspace > 0) {
        freespace = SHA512_BLOCK_LENGTH - usedspace;

        if (len >= freespace) {
          /* Fill the buffer completely and process it */
          MEMCPY_BCOPY(&context->buffer[usedspace], data, freespace);
          ADDINC128(context->bitcount, freespace << 3);
          len -= freespace;
          data += freespace;
          SHA512_Transform(context,
              const_cast<sha2_word64 *>(
                  reinterpret_cast<const sha2_word64 *>(context->buffer)));
        } else {
          MEMCPY_BCOPY(&context->buffer[usedspace], data, len);
          ADDINC128(context->bitcount, len << 3);
          usedspace = freespace = 0;
          return;
        }
      }
      while (len >= SHA512_BLOCK_LENGTH) {
        SHA512_Transform(
            context, const_cast<sha2_word64 *>(
                         reinterpret_cast<const sha2_word64 *>(data)));
        ADDINC128(context->bitcount, SHA512_BLOCK_LENGTH << 3);
        len -= SHA512_BLOCK_LENGTH;
        data += SHA512_BLOCK_LENGTH;
      }
      if (len > 0) {
        /* There's left-overs, so save 'em */
        MEMCPY_BCOPY(context->buffer, data, len);
        ADDINC128(context->bitcount, len << 3);
      }
      /* Clean up: */
      usedspace = freespace = 0;
    }


    char *SHA384_End(
        HL_SHA_384_CTX *context, char buffer[SHA384_DIGEST_STRING_LENGTH]) {
      sha2_byte digest[SHA384_DIGEST_LENGTH], *d = digest;

      /* Sanity check: */
      assert(context != (HL_SHA_384_CTX *) 0);

      if (buffer != static_cast<char *>(0)) {
        SHA384_Final(digest, context);

        for (unsigned int i = 0; i < SHA384_DIGEST_LENGTH; i++) {
          *buffer++ = sha2_hex_digits[(*d & 0xf0) >> 4];
          *buffer++ = sha2_hex_digits[*d & 0x0f];
          d++;
        }
        *buffer = static_cast<char>(0);
      } else {
        MEMSET_BZERO(context, sizeof(context));
      }
      MEMSET_BZERO(digest, SHA384_DIGEST_LENGTH);
      return buffer;
    }

    char *SHA512_End(
        HL_SHA512_CTX *context, char buffer[SHA512_DIGEST_STRING_LENGTH]) {
      sha2_byte digest[SHA512_DIGEST_LENGTH], *d = digest;

      /* Sanity check: */
      assert(context != static_cast<HL_SHA512_CTX *>(0));

      if (buffer != static_cast<char *>(0)) {
        SHA512_Final(digest, context);

        for (unsigned int i = 0; i < SHA512_DIGEST_LENGTH; i++) {
          *buffer++ = sha2_hex_digits[(*d & 0xf0) >> 4];
          *buffer++ = sha2_hex_digits[*d & 0x0f];
          d++;
        }
        *buffer = (char) 0;
      } else {
        MEMSET_BZERO(context, sizeof(context));
      }
      MEMSET_BZERO(digest, SHA512_DIGEST_LENGTH);
      return buffer;
    }
  };


  class hashwrapper {
   protected:
    virtual std::string hashIt(void) = 0;

    virtual std::string convToString(unsigned char *data) = 0;

    virtual void updateContext(unsigned char *data, unsigned int len) = 0;

    virtual void resetContext(void) = 0;


   public:
    hashwrapper(void) {
    }

    virtual ~hashwrapper(void){};

    virtual std::string getHashFromString(std::string text) {
      resetContext();
      updateContext((unsigned char *) text.c_str(), text.length());
      return this->hashIt();
    }


    virtual std::string getHashFromFile(std::istream &file) {
      char buffer[1];
      resetContext();
      while (file.read(buffer, 1)) {
        std::streamsize len = file.gcount();
        updateContext(reinterpret_cast<unsigned char *>(buffer), len);
      }
      return hashIt();
    }


    virtual std::string getHashFromFile(FILE *file) {
      unsigned char buffer[1024];
      resetContext();
      while (int len = fread(buffer, 1, 1024, file)) {
        updateContext(buffer, len);
      }
      return hashIt();
    }


    virtual std::string getHashFromFile(std::string filename) {
      FILE *file;
      if ((file = fopen(filename.c_str(), "rb")) == NULL) {
        throw hlException(
            HL_FILE_READ_ERROR, "Cannot read file \"" + filename + "\".");
      }
      std::string hash = getHashFromFile(file);
      fclose(file);
      return hash;
    }
  };  // end of hashwrapper


  class sha256wrapper : public hashwrapper {
   private:
    std::unique_ptr<SHA256> sha256;
    HL_SHA256_CTX context;


    virtual std::string hashIt(void) {
      sha2_byte buff[SHA256_DIGEST_STRING_LENGTH];
      sha256->SHA256_End(&context, (char *) buff);
      return convToString(buff);
    }


    virtual std::string convToString(unsigned char *data) {
      return std::string((const char *) data);
    }


    virtual void updateContext(unsigned char *data, unsigned int len) {
      this->sha256->SHA256_Update(&context, data, len);
    }


    virtual void resetContext(void) {
      sha256->SHA256_Init(&context);
    }


   public:
    sha256wrapper() {
      this->sha256 = std::make_unique<SHA256>();
    }


    virtual ~sha256wrapper() {
      this->sha256.release();
    }
  };  // end of sha256wrapper


  class sha1wrapper : public hashwrapper {
   protected:
    std::unique_ptr<SHA1> sha1;
    HL_SHA1_CTX context;

    virtual std::string hashIt(void) {
      hl_uint8 Message_Digest[20];
      sha1->SHA1Result(&context, Message_Digest);

      return convToString(Message_Digest);
    }


    virtual std::string convToString(unsigned char *data) {
      std::ostringstream os;
      for (unsigned int i = 0; i < 20; ++i) {
        os.width(2);
        os.fill('0');
        os << std::hex << static_cast<unsigned int>(data[i]);
      }
      return os.str();
    }

    virtual void updateContext(unsigned char *data, unsigned int len) {
      sha1->SHA1Input(&context, data, len);
    }


    virtual void resetContext(void) {
      sha1->SHA1Reset(&context);
    }


   public:
    sha1wrapper() {
      this->sha1 = std::make_unique<SHA1>();
    }


    virtual ~sha1wrapper() {
      this->sha1.release();
    }
  };  // end of sha1wrapper


  class md5wrapper : public hashwrapper {
   protected:
    std::unique_ptr<MD5> md5;
    HL_MD5_CTX ctx;

    virtual std::string hashIt(void) {
      unsigned char buff[16] = "";
      md5->MD5Final((unsigned char *) buff, &ctx);
      return convToString(buff);
    }

    virtual std::string convToString(unsigned char *data) {
      std::ostringstream os;
      for (unsigned int i = 0; i < 16; ++i) {
        os.width(2);
        os.fill('0');
        os << std::hex << static_cast<unsigned int>(data[i]);
      }


      return os.str();
    }


    virtual void updateContext(unsigned char *data, unsigned int len) {
      md5->MD5Update(&ctx, data, len);
    }


    virtual void resetContext(void) {
      md5->MD5Init(&ctx);
    }


   public:
    md5wrapper() {
      md5 = std::make_unique<MD5>();
    }


    virtual ~md5wrapper() {
      this->md5.release();
    }
  };  // end of md5wrapper


  class sha384wrapper : public hashwrapper {
   private:
    std::unique_ptr<SHA2ext> sha384;

    HL_SHA_384_CTX context;

    virtual std::string hashIt(void) {
      sha2_byte buff[SHA384_DIGEST_STRING_LENGTH];
      sha384->SHA384_End(&context, (char *) buff);
      return convToString(buff);
    }

    virtual std::string convToString(unsigned char *data) {
      return std::string((const char *) data);
    }

    virtual void updateContext(unsigned char *data, unsigned int len) {
      this->sha384->SHA384_Update(&context, data, len);
    }

    virtual void resetContext(void) {
      sha384->SHA384_Init(&context);
    }

   public:
    sha384wrapper() {
      this->sha384 = std::make_unique<SHA2ext>();
    }

    virtual ~sha384wrapper() {
      this->sha384.reset();
    }
  };  // end of sha384wrapper


  class sha512wrapper : public hashwrapper {
   private:
    std::unique_ptr<SHA2ext> sha512;
    HL_SHA512_CTX context;

    virtual std::string hashIt(void) {
      sha2_byte buff[SHA512_DIGEST_STRING_LENGTH];
      sha512->SHA512_End(&context, (char *) buff);
      return convToString(buff);
    }

    virtual std::string convToString(unsigned char *data) {
      return std::string((const char *) data);
    }

    virtual void updateContext(unsigned char *data, unsigned int len) {
      this->sha512->SHA512_Update(&context, data, len);
    }

    virtual void resetContext(void) {
      sha512->SHA512_Init(&context);
    }


   public:
    sha512wrapper() {
      this->sha512 = std::make_unique<SHA2ext>();
    }


    virtual ~sha512wrapper() {
      this->sha512.reset();
    }
  };  // end of sha512wrapper

  template<typename T>
  constexpr std::string abstract_hash(std::string input) {
    std::unique_ptr<hashwrapper> myWrapper = std::make_unique<T>();
    std::string hash = myWrapper->getHashFromString(input);
    myWrapper.reset();
    return hash;
  }


  template<typename T>
  constexpr std::string abstract_hash(FILE *input) {
    std::unique_ptr<hashwrapper> myWrapper = std::make_unique<T>();
    std::string hash = myWrapper->getHashFromFile(input);
    myWrapper.reset();
    return hash;
  }


  template<typename T>
  constexpr std::string abstract_hash(std::istream &input) {
    auto state = input.tellg();
    input.clear();
    input.seekg(0, std::fstream::beg);
    std::unique_ptr<hashwrapper> myWrapper = std::make_unique<T>();
    std::string hash = myWrapper->getHashFromFile(input);
    myWrapper.reset();
    input.seekg(state);
    return hash;
  }
}  // end of namespace ___

/**
 * Usable library starts here.
 */

inline std::string MD5(std::string input) {
  return ___::abstract_hash<___::md5wrapper>(input);
}


inline std::string MD5(FILE *input) {
  return ___::abstract_hash<___::md5wrapper>(input);
}


inline std::string MD5(std::istream &input) {
  return ___::abstract_hash<___::md5wrapper>(input);
}


inline std::string SHA1(std::string input) {
  return ___::abstract_hash<___::sha1wrapper>(input);
}


inline std::string SHA1(FILE *input) {
  return ___::abstract_hash<___::sha1wrapper>(input);
}


inline std::string SHA1(std::istream &input) {
  return ___::abstract_hash<___::sha1wrapper>(input);
}


inline std::string SHA256(std::string input) {
  return ___::abstract_hash<___::sha256wrapper>(input);
}


inline std::string SHA256(FILE *input) {
  return ___::abstract_hash<___::sha256wrapper>(input);
}


inline std::string SHA256(std::istream &input) {
  return ___::abstract_hash<___::sha256wrapper>(input);
}


inline std::string SHA384(std::string input) {
  return ___::abstract_hash<___::sha384wrapper>(input);
}


inline std::string SHA384(FILE *input) {
  return ___::abstract_hash<___::sha384wrapper>(input);
}


inline std::string SHA384(std::istream &input) {
  return ___::abstract_hash<___::sha384wrapper>(input);
}


inline std::string SHA512(std::string input) {
  return ___::abstract_hash<___::sha512wrapper>(input);
}


inline std::string SHA512(FILE *input) {
  return ___::abstract_hash<___::sha512wrapper>(input);
}


inline std::string SHA512(std::istream &input) {
  return ___::abstract_hash<___::sha512wrapper>(input);
}

}  // namespace HO_Hashlibpp

#endif /* end of include guard: HO_HASHLIBPP_LIB_HO_HASHLIBPP__H__ */