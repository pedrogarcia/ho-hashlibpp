/* The copyright in this software is being made available under the BSD
 * License, included below. This software may be subject to other third party
 * and contributor rights, including patent rights, and no such rights are
 * granted under this license.
 *
 * Copyright (c) 2020, Pedro Garcia Freitas
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  * Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *  * Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

/** \file     hashlib_strings_tests.cpp
 *  \brief    Tests of HO_Hashlib using C FILE as input.
 *  \author   Pedro Garcia Freitas <pedrogarcia@ieee.org>
 *  \date     2020-04-04
 */

#include <string>
#include "Lib/ho-hashlibpp.h"
#include "gtest/gtest.h"


std::string resources_path = "../resources";


class HO_HashlibppFileTests : public ::testing::Test {
 protected:
  std::string input_filename;
  FILE* file;

  void SetUp() override {
    input_filename = resources_path + "/img.ppm";
    file = fopen(input_filename.c_str(), "rb");
  }
};


TEST_F(HO_HashlibppFileTests, MD5FromFileTest) {
  std::string hash = HO_Hashlibpp::MD5(file);
  EXPECT_EQ("c6b51ef22770b3a5151807d2078d7c64", hash);
}


TEST_F(HO_HashlibppFileTests, SHA1FromFileTest) {
  std::string hash = HO_Hashlibpp::SHA1(file);
  EXPECT_EQ("32504e48aa1e40aafc582379aca3bd6061d2a119", hash);
}


TEST_F(HO_HashlibppFileTests, SHA256FromFileTest) {
  std::string hash = HO_Hashlibpp::SHA256(file);
  EXPECT_EQ(
      "4e349d4a142e433bbe2779e67e8c3db584edf298cb1f465d0d8ff75760c96ca7", hash);
}


TEST_F(HO_HashlibppFileTests, SHA384FromFileTest) {
  std::string hash = HO_Hashlibpp::SHA384(file);
  EXPECT_EQ(
      "28ad22d313274163477b88972b6597380ffd61bef3cee15ad78cd28d2e1b1e177dafd4f4"
      "acaea57ed9b6f0d5093be896",
      hash);
}


TEST_F(HO_HashlibppFileTests, SHA512FromFileTest) {
  std::string hash = HO_Hashlibpp::SHA512(file);
  EXPECT_EQ(
      "889d1ab2fcdca4bb8f979e5cdf8542e27c6bcfbd6998995dd8e439bde6203ddee547889e"
      "580a16c04894591858aec135ddfce5d0b1b132f80a3f29883287f82a",
      hash);
}


int main(int argc, char* argv[]) {
  testing::InitGoogleTest(&argc, argv);
  if (argc > 1) {
    resources_path = std::string(argv[1]);
  }
  return RUN_ALL_TESTS();
}
